package in.assamtetguide.com.repository;

import android.util.Log;

import com.google.gson.Gson;

import in.assamtetguide.com.domain.model.MainDataWrapper;
import in.assamtetguide.com.domain.model.SubjectsWrapper;
import in.assamtetguide.com.domain.model.TopicsWrapper;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class AppRepositoryImpl {

    AppRepository mRepository;

    public AppRepositoryImpl() {
        mRepository = APIclient.createService(AppRepository.class);
    }

    public MainDataWrapper getHomeData() {
        MainDataWrapper mainDataWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.getHomeData();

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    mainDataWrapper = null;
                }else{
                    mainDataWrapper = gson.fromJson(responseBody, MainDataWrapper.class);
                }
            } else {
                mainDataWrapper = null;
            }
        }catch (Exception e){
            mainDataWrapper = null;
        }
        return mainDataWrapper;
    }

    public SubjectsWrapper fetchSubjects(int pageId) {
        SubjectsWrapper subjectsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchSubjects(pageId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    subjectsWrapper = null;
                }else{
                    subjectsWrapper = gson.fromJson(responseBody, SubjectsWrapper.class);
                }
            } else {
                subjectsWrapper = null;
            }
        }catch (Exception e){
            subjectsWrapper = null;
        }
        return subjectsWrapper;
    }

    public TopicsWrapper fetchTopics(int pageNo, int subjectId) {
        TopicsWrapper topicsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchTopics(pageNo, subjectId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    topicsWrapper = null;
                }else{
                    topicsWrapper = gson.fromJson(responseBody, TopicsWrapper.class);
                }
            } else {
                topicsWrapper = null;
            }
        }catch (Exception e){
            topicsWrapper = null;
        }
        return topicsWrapper;
    }

}
