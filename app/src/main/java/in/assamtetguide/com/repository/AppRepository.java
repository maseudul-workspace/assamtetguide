package in.assamtetguide.com.repository;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface AppRepository {

    @GET("app_load_api.php")
    Call<ResponseBody> getHomeData();

    @POST("fetch_subjects.php")
    @FormUrlEncoded
    Call<ResponseBody> fetchSubjects(@Field("paper_id") int paperId) ;

    @POST("topic_list.php")
    @FormUrlEncoded
    Call<ResponseBody> fetchTopics(@Field("page_no") int pageNo,
                                   @Field("subject_id") int subjectId
                                   ) ;

}
