package in.assamtetguide.com.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MainData {

    @SerializedName("slider")
    @Expose
    public Slider[] sliders;
}
