package in.assamtetguide.com.domain.interactors.impl;

import in.assamtetguide.com.domain.executor.Executor;
import in.assamtetguide.com.domain.executor.MainThread;
import in.assamtetguide.com.domain.interactors.FetchTopicsInteractor;
import in.assamtetguide.com.domain.interactors.base.AbstractInteractor;
import in.assamtetguide.com.domain.model.Subjects;
import in.assamtetguide.com.domain.model.Topics;
import in.assamtetguide.com.domain.model.TopicsWrapper;
import in.assamtetguide.com.repository.AppRepositoryImpl;

public class FetchTopicsInteractorImpl extends AbstractInteractor implements FetchTopicsInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    int pageNo;
    int subjectId;

    public FetchTopicsInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository, int pageNo, int subjectId) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.pageNo = pageNo;
        this.subjectId = subjectId;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingTopicsFail(errorMsg);
            }
        });
    }

    private void postMessage(Topics[] topics, int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingTopicsSuccess(topics, totalPage);
            }
        });
    }

    @Override
    public void run() {
        final TopicsWrapper topicsWrapper = mRepository.fetchTopics(pageNo, subjectId);
        if (topicsWrapper == null) {
            notifyError("No Topics Found");
        } else if (!topicsWrapper.status) {
            notifyError(topicsWrapper.message);
        } else {
            postMessage(topicsWrapper.topics, topicsWrapper.totalPage);
        }
    }
}
