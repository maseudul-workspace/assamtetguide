package in.assamtetguide.com.domain.interactors;

import in.assamtetguide.com.domain.model.MainData;

public interface GetMainDataInteractor {
    interface Callback {
        void onGettingDataSuccess(MainData mainData);
        void onGettingDataFail();
    }
}
