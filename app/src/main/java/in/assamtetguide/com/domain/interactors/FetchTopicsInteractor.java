package in.assamtetguide.com.domain.interactors;

import in.assamtetguide.com.domain.model.Topics;
import in.assamtetguide.com.domain.model.TopicsWrapper;

public interface FetchTopicsInteractor {

    interface Callback {
        void onGettingTopicsSuccess(Topics[] topics, int totalPage);
        void onGettingTopicsFail(String errorMsg);
    }

}
