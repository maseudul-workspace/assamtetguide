package in.assamtetguide.com.domain.interactors.impl;

import in.assamtetguide.com.domain.executor.Executor;
import in.assamtetguide.com.domain.executor.MainThread;
import in.assamtetguide.com.domain.interactors.GetMainDataInteractor;
import in.assamtetguide.com.domain.interactors.base.AbstractInteractor;
import in.assamtetguide.com.domain.model.MainData;
import in.assamtetguide.com.domain.model.MainDataWrapper;
import in.assamtetguide.com.repository.AppRepositoryImpl;

public class GetMainDataInteractorImpl extends AbstractInteractor implements GetMainDataInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;

    public GetMainDataInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingDataFail();
            }
        });
    }

    private void postMessage(MainData mainData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingDataSuccess(mainData);
            }
        });
    }

    @Override
    public void run() {
        final MainDataWrapper mainDataWrapper = mRepository.getHomeData();
        if (mainDataWrapper == null) {
            notifyError();
        } else if (!mainDataWrapper.status) {
            notifyError();
        } else {
            postMessage(mainDataWrapper.mainData);
        }
    }
}
