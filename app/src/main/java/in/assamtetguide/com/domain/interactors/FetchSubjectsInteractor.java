package in.assamtetguide.com.domain.interactors;

import in.assamtetguide.com.domain.model.Subjects;

public interface FetchSubjectsInteractor {
    interface Callback {
       void onFetchingSubjectsSuccess(Subjects[] subjects);
       void onFetchingSubjectsFail(String errorMsg);
    }
}
