package in.assamtetguide.com.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopicsWrapper {

    @SerializedName("status")
    @Expose
    public Boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("code")
    @Expose
    public int code;

    @SerializedName("total_page")
    @Expose
    public int totalPage;

    @SerializedName("data")
    @Expose
    public Topics[] topics;

}
