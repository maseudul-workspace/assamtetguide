package in.assamtetguide.com.domain.interactors.impl;

import in.assamtetguide.com.domain.executor.Executor;
import in.assamtetguide.com.domain.executor.MainThread;
import in.assamtetguide.com.domain.interactors.FetchSubjectsInteractor;
import in.assamtetguide.com.domain.interactors.base.AbstractInteractor;
import in.assamtetguide.com.domain.model.MainData;
import in.assamtetguide.com.domain.model.Subjects;
import in.assamtetguide.com.domain.model.SubjectsWrapper;
import in.assamtetguide.com.presentation.presenters.base.AbstractPresenter;
import in.assamtetguide.com.repository.AppRepositoryImpl;

public class FetchSubjectsInteractorImpl extends AbstractInteractor implements FetchSubjectsInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    int pageId;

    public FetchSubjectsInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository, int pageId) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.pageId = pageId;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchingSubjectsFail(errorMsg);
            }
        });
    }

    private void postMessage(Subjects[] subjects){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchingSubjectsSuccess(subjects);
            }
        });
    }

    @Override
    public void run() {
        final SubjectsWrapper subjectsWrapper = mRepository.fetchSubjects(pageId);

        if (subjectsWrapper == null) {
            notifyError("Something went wrong");
        } else if (!subjectsWrapper.status) {
            notifyError("Something went wrong");
        } else {
            postMessage(subjectsWrapper.subjects);
        }

    }
}
