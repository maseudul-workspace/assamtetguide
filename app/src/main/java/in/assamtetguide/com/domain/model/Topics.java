package in.assamtetguide.com.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Topics {

    @SerializedName("name_eng")
    @Expose
    public String nameEng;

    @SerializedName("name_ben")
    @Expose
    public String nameBen;

    @SerializedName("file_name")
    @Expose
    public String fileName;

}
