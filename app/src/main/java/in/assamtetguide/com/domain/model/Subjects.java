package in.assamtetguide.com.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Subjects {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name_eng")
    @Expose
    public String nameEng;

    @SerializedName("name_bem")
    @Expose
    public String nameBen;

}
