package in.assamtetguide.com.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

import com.google.firebase.messaging.RemoteMessage;

import in.assamtetguide.com.domain.model.NotificationModel;
import in.assamtetguide.com.presentation.ui.activities.TopicsActivity;
import in.assamtetguide.com.util.NotificationUtils;

/**
 * Created by Raj on 29-06-2019.
 */

public class AppFirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        String message = "";
        try{
            message = remoteMessage.getData().get("message");
        }catch (NullPointerException e){
            message = "New Topic Added";
        }

        String title;
        try{
            title = remoteMessage.getData().get("title");
        }catch (NullPointerException e){
            title = "Assam TET Guide";
        }

//        String bookingIdString = remoteMessage.getData().get("booking_id");
//        String apiKey = remoteMessage.getData().get("api_key");
//        int bookingIdInt = Integer.parseInt(bookingIdString);
//
        String subjectId = remoteMessage.getData().get("subject_id");
        int subjectIdInt = Integer.parseInt(subjectId);
        Intent resultIntent;

        resultIntent = new Intent(this, TopicsActivity.class);
        resultIntent.putExtra("subjectId", subjectIdInt);

        NotificationModel notificationModel = new NotificationModel(title, message);
        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
        notificationUtils.displayNotification(notificationModel, resultIntent);

    }

    @Override
    public void onTaskRemoved(Intent rootIntent){
        Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());
        restartServiceIntent.setPackage(getPackageName());

        PendingIntent restartServicePendingIntent = PendingIntent.getService(getApplicationContext(), 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + 1000,
                restartServicePendingIntent);

        super.onTaskRemoved(rootIntent);
    }



}
