package in.assamtetguide.com.services;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessaging;

/**
 * Created by Raj on 29-06-2019.
 */

public class AppFirebaseInstanceIdService extends FirebaseInstanceIdService {

    private static final String TOPIC_GLOBAL = "global";

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        FirebaseMessaging.getInstance().subscribeToTopic(TOPIC_GLOBAL);
    }
}
