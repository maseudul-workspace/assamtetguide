package in.assamtetguide.com.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import in.assamtetguide.com.R;
import in.assamtetguide.com.domain.executor.impl.ThreadExecutor;
import in.assamtetguide.com.presentation.presenters.TopicsPresenter;
import in.assamtetguide.com.presentation.presenters.impl.TopicPresenterImpl;
import in.assamtetguide.com.presentation.ui.adapters.TopicsAdapter;
import in.assamtetguide.com.threading.MainThreadImpl;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TopicsActivity extends AppCompatActivity implements TopicsPresenter.View {

    ProgressDialog progressDialog;
    String[] appPremisions = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };
    private static final int PERMISSIONS_REQUEST_CODE = 1240;
    TopicPresenterImpl mPresenter;
    String finalUrl = "";
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int pageNo = 1;
    int totalPage = 1;
    LinearLayoutManager layoutManager;
    int subjectId;
    @BindView(R.id.recycler_view_topics)
    RecyclerView recyclerView;
    String filName;
    @BindView(R.id.progress_bar)
    View progressBar;
    @BindView(R.id.pagination_progress_bar)
    View paginationProgressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topics);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Topics");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        subjectId  = getIntent().getIntExtra("subjectId", 0);
        initialisePresenter();
        mPresenter.fetchTopics(pageNo, subjectId, "refresh");
        progressDialog = new ProgressDialog(this);
    }

    public void initialisePresenter() {
        mPresenter = new TopicPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void loadAdapter(TopicsAdapter topicsAdapter, int totalPage) {
        this.totalPage = totalPage;
        recyclerView.setVisibility(View.VISIBLE);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(topicsAdapter);
        DividerItemDecoration itemDecor = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(itemDecor);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        showPaginationProgressBar();
                        mPresenter.fetchTopics(pageNo, subjectId, "");
                    }
                }
            }
        });
    }

    @Override
    public void downloadFile(String fileName) {
        this.filName = fileName;
        if(checkAndRequestPermissions()){
            new DownloadFileAsync().execute(filName);
        }
    }

    @Override
    public void viewFile(String fileName) {
        Intent intent = new Intent(this, PdfPreviewActivity.class);
        intent.putExtra("PDF_URL", "http://tet.wonderlandnationalschool.com/uploads/topic_pdf/" + fileName);
        startActivity(intent);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showPaginationProgressBar() {
        paginationProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hidePaginationProgressBar() {
        paginationProgressBar.setVisibility(View.GONE);
    }

    class DownloadFileAsync extends AsyncTask<String, String, String> {

        Boolean isError = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setTitle("Downloading file");
            progressDialog.setMax(100);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            int count;
            try{
                URL url = new URL("http://tet.wonderlandnationalschool.com/uploads/topic_pdf/" + strings[0]);
                URLConnection urlConnection = url.openConnection();
                urlConnection.setRequestProperty("Accept-Encoding", "identity");
                urlConnection.connect();
                int length = urlConnection.getContentLength();
                InputStream inputStream = new BufferedInputStream(url.openStream());
                File directory = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/AssamTETGuide");
                if (!directory.isDirectory()) {
                    directory.mkdirs();
                }
                File file = new File(directory, filName);
                OutputStream outputStream = new FileOutputStream(file);
                byte data[] = new byte[1024];
                long total = 0;
                while((count = inputStream.read(data)) != -1){
                    total = total + count;
                    publishProgress("" + (int)((total*100)/length));
                    outputStream.write(data, 0, count);
                }
                outputStream.flush();
                outputStream.close();
                inputStream.close();
            }catch (Exception e){
                progressDialog.dismiss();
                isError = true;
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            progressDialog.setProgress(Integer.parseInt(values[0]));
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if(isError){
                Toasty.error(getApplicationContext(), "Download failed",Toast.LENGTH_SHORT, true).show();
            }else{
                Toasty.success(getApplicationContext(), "Download Success: Check inside AssamTETGuide folder of your storage", Toast.LENGTH_SHORT, true).show();
            }

        }
    }

    public boolean checkAndRequestPermissions(){
        List<String> listPermissionsNeeded = new ArrayList<>();
        for(String perm: appPremisions){
            if(ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED){
                listPermissionsNeeded.add(perm);
            }
        }
        if(!listPermissionsNeeded.isEmpty()){
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PERMISSIONS_REQUEST_CODE);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == PERMISSIONS_REQUEST_CODE){
            HashMap<String, Integer> permissionResults = new HashMap<>();
            int deniedCount = 0;
            for (int i = 0; i<grantResults.length; i++){
                if( grantResults[i] == PackageManager.PERMISSION_DENIED ){
                    permissionResults.put(permissions[i], grantResults[i]);
                    deniedCount++;
                }
            }

            if(deniedCount == 0){
//               download file
                new DownloadFileAsync().execute(finalUrl);
            }

            else{
                for (Map.Entry<String, Integer> entry : permissionResults.entrySet()){
                    String permName = entry.getKey();
                    int permResult = entry.getValue();
                    if(ActivityCompat.shouldShowRequestPermissionRationale(this, permName)){
                        this.showAlertDialog("", "This app needs read and write storage permissions",
                                "Yes, Grant permissions",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        checkAndRequestPermissions();
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required to download file", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                    }
                    else {
                        this.showAlertDialog("", "You have denied some permissions. Allow all permissions to download file at [Setting] > [Permissions]",
                                "Go to settings",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                                Uri.fromParts("package", getPackageName(), null));
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required to download file", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                        break;
                    }
                }
            }

        }
    }

    public AlertDialog showAlertDialog(
            String title, String msg, String positiveLabel,
            DialogInterface.OnClickListener positiveOnClick,
            String negativeLabel, DialogInterface.OnClickListener negativeOnClick,
            boolean isCancelable
    ){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setCancelable(isCancelable);
        builder.setMessage(msg);
        builder.setPositiveButton(positiveLabel, positiveOnClick);
        builder.setNegativeButton(negativeLabel, negativeOnClick);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        return alertDialog;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
