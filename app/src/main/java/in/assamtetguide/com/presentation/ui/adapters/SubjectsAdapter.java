package in.assamtetguide.com.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import in.assamtetguide.com.R;
import in.assamtetguide.com.domain.model.Subjects;

public class SubjectsAdapter extends RecyclerView.Adapter<SubjectsAdapter.ViewHolder> {

    public interface Callback {
        void onViewClicked(int subjectId);
    }

    Context mContext;
    Subjects[] subjects;
    Callback mCallback;

    public SubjectsAdapter(Context mContext, Subjects[] subjects, Callback callback) {
        this.mContext = mContext;
        this.subjects = subjects;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_subjects, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewBookTitle.setText(subjects[position].nameEng);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(subjects[position].id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return subjects.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_book_title)
        TextView txtViewBookTitle;
        @BindView(R.id.card_view)
        View cardView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
