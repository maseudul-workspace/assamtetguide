package in.assamtetguide.com.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.assamtetguide.com.R;
import in.assamtetguide.com.domain.executor.impl.ThreadExecutor;
import in.assamtetguide.com.domain.model.MainData;
import in.assamtetguide.com.domain.model.Slider;
import in.assamtetguide.com.presentation.presenters.MainPresenter;
import in.assamtetguide.com.presentation.presenters.impl.MainPresenterImpl;
import in.assamtetguide.com.presentation.ui.util.GlideHelper;
import in.assamtetguide.com.threading.MainThreadImpl;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.google.firebase.iid.FirebaseInstanceId;

public class MainActivity extends AppCompatActivity implements MainPresenter.View {

    @BindView(R.id.img_view_lt)
    ImageView imgViewLT;
    @BindView(R.id.img_view_me)
    ImageView imgViewME;
    @BindView(R.id.viewflipper_main)
    ViewFlipper viewFlipper;
    MainPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setUpFields();
        setUpFlipper();
        initialisePresenter();
        mPresenter.getMainData();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e("LogMsg", "Refresh Token: " + refreshedToken);

    }

    public void initialisePresenter() {
        mPresenter = new MainPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this::loadData);
    }

    public void setUpFields() {
        GlideHelper.setImageView(this, imgViewLT, "https://www.eabooks.com.au/WebRoot/ecshared01/Shops/eabooks/MediaGallery/Background_books.jpg");
        GlideHelper.setImageView(this, imgViewME, "http://mgt.sjp.ac.lk/ent/wp-content/uploads/2015/10/Books.jpg");
    }

    public void setUpFlipper(){
        viewFlipper.setFlipInterval(3000);
        viewFlipper.setAutoStart(true);
        viewFlipper.setInAnimation(this, android.R.anim.slide_in_left);
        viewFlipper.setOutAnimation(this, android.R.anim.slide_out_right);
    }

    public void flipperImages(String image){
        ImageView imageView = new ImageView(this);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        GlideHelper.setImageView(this, imageView, "http://tet.wonderlandnationalschool.com/uploads/slider/"  + image);
        viewFlipper.addView(imageView);
    }

    @Override
    public void loadData(MainData mainData) {
        Slider slider[] = mainData.sliders;
        for (int i = 0; i < slider.length; i++) {
            flipperImages(slider[i].image);
        }
    }

    @OnClick(R.id.card_view_me) void onMEClicked() {
        Intent intent = new Intent(this, SubjectsActivity.class);
        intent.putExtra("pageId", 2);
        startActivity(intent);
    }

    @OnClick(R.id.card_view_lp) void onLPClicked() {
        Intent intent = new Intent(this, SubjectsActivity.class);
        intent.putExtra("pageId", 1);
        startActivity(intent);
    }

}
