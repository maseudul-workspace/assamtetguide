package in.assamtetguide.com.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;

import in.assamtetguide.com.domain.executor.Executor;
import in.assamtetguide.com.domain.executor.MainThread;
import in.assamtetguide.com.domain.interactors.GetMainDataInteractor;
import in.assamtetguide.com.domain.interactors.impl.GetMainDataInteractorImpl;
import in.assamtetguide.com.domain.model.MainData;
import in.assamtetguide.com.presentation.presenters.MainPresenter;
import in.assamtetguide.com.presentation.presenters.base.AbstractPresenter;
import in.assamtetguide.com.repository.AppRepositoryImpl;

public class MainPresenterImpl extends AbstractPresenter implements MainPresenter, GetMainDataInteractor.Callback {

    Context mContext;
    MainPresenter.View mView;
    GetMainDataInteractorImpl mInteractor;

    public MainPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void getMainData() {
        mInteractor = new GetMainDataInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl());
        mInteractor.execute();
    }

    @Override
    public void onGettingDataSuccess(MainData mainData) {
        mView.loadData(mainData);
    }

    @Override
    public void onGettingDataFail() {

    }
}
