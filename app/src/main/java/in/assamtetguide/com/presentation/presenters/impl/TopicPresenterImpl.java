package in.assamtetguide.com.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.assamtetguide.com.domain.executor.Executor;
import in.assamtetguide.com.domain.executor.MainThread;
import in.assamtetguide.com.domain.interactors.FetchTopicsInteractor;
import in.assamtetguide.com.domain.interactors.impl.FetchSubjectsInteractorImpl;
import in.assamtetguide.com.domain.interactors.impl.FetchTopicsInteractorImpl;
import in.assamtetguide.com.domain.model.Topics;
import in.assamtetguide.com.presentation.presenters.TopicsPresenter;
import in.assamtetguide.com.presentation.presenters.base.AbstractPresenter;
import in.assamtetguide.com.presentation.ui.adapters.TopicsAdapter;
import in.assamtetguide.com.repository.AppRepositoryImpl;

public class TopicPresenterImpl extends AbstractPresenter implements TopicsPresenter, FetchTopicsInteractor.Callback, TopicsAdapter.Callback {

    Context mContext;
    TopicsPresenter.View mView;
    Topics[] newTopics;
    TopicsAdapter adapter;
    FetchTopicsInteractorImpl mInteractor;

    public TopicPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchTopics(int pageNo, int subjectId, String type) {
        if(type.equals("refresh")){
            newTopics = null;
        }
        mInteractor = new FetchTopicsInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), pageNo, subjectId);
        mInteractor.execute();
    }

    @Override
    public void onGettingTopicsSuccess(Topics[] topics, int totalPage) {
        Topics[] tempTopics;
        tempTopics = newTopics;
        try{
            int len1 = tempTopics.length;
            int len2 = topics.length;
            newTopics = new Topics[len1 + len2];
            System.arraycopy(tempTopics, 0, newTopics, 0, len1);
            System.arraycopy(topics, 0, newTopics, len1, len2);
            adapter.updateDataSet(newTopics);
            mView.hidePaginationProgressBar();
        }catch (NullPointerException e){
            newTopics = topics;
            adapter = new TopicsAdapter(mContext, topics, this);
            mView.loadAdapter(adapter, totalPage);
            mView.hideProgressBar();
        }
    }

    @Override
    public void onGettingTopicsFail(String errorMsg) {
        mView.hideProgressBar();
        mView.hidePaginationProgressBar();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_LONG, true).show();
    }

    @Override
    public void onDownloadClicked(String fileName) {
        mView.downloadFile(fileName);
    }

    @Override
    public void onViewClicked(String fileName) {
        mView.viewFile(fileName);
    }
}
