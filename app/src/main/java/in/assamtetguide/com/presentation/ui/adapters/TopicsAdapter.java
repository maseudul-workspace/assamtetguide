package in.assamtetguide.com.presentation.ui.adapters;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import in.assamtetguide.com.R;
import in.assamtetguide.com.domain.model.Topics;

public class TopicsAdapter extends RecyclerView.Adapter<TopicsAdapter.ViewHolder> {

    public interface Callback {
        void onDownloadClicked(String fileName);
        void onViewClicked(String fileName);
    }

    Context mContext;
    Topics[] topics;
    Callback mCallback;

    public TopicsAdapter(Context mContext, Topics[] topics, Callback mCallback) {
        this.mContext = mContext;
        this.topics = topics;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_topics, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewTopicTitle.setText(Html.fromHtml(topics[position].nameEng) + " - " + topics[position].nameBen);
        holder.btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onDownloadClicked(topics[position].fileName);
            }
        });
        holder.btnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(topics[position].fileName);
            }
        });
    }

    @Override
    public int getItemCount() {
        return topics.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_topic_title)
        TextView txtViewTopicTitle;
        @BindView(R.id.btn_view)
        MaterialButton btnView;
        @BindView(R.id.btn_download)
        MaterialButton btnDownload;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateDataSet(Topics[] topics) {
        this.topics = topics;
        notifyDataSetChanged();
    }

}
