package in.assamtetguide.com.presentation.presenters;

import in.assamtetguide.com.presentation.ui.adapters.SubjectsAdapter;

public interface SubjectsPresenter {

    void fetchSubjects(int pageId);

    interface View {
        void loadAdapter(SubjectsAdapter adapter);
        void goToTopicsActivity(int subjectId);
        void hideProgressBar();
    }

}
