package in.assamtetguide.com.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import in.assamtetguide.com.R;
import in.assamtetguide.com.domain.executor.impl.ThreadExecutor;
import in.assamtetguide.com.domain.model.Subjects;
import in.assamtetguide.com.presentation.presenters.SubjectsPresenter;
import in.assamtetguide.com.presentation.presenters.impl.SubjectsPresenterImpl;
import in.assamtetguide.com.presentation.ui.adapters.SubjectsAdapter;
import in.assamtetguide.com.threading.MainThreadImpl;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

public
class SubjectsActivity extends AppCompatActivity implements SubjectsPresenter.View {

    @BindView(R.id.recycler_view_subjects)
    RecyclerView recyclerViewSubjects;
    SubjectsPresenterImpl mPresenter;
    int pageId;
    @BindView(R.id.progress_bar)
    View progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subjects);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Subjects");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initialisePresenter();
        pageId = getIntent().getIntExtra("pageId", 0);
        mPresenter.fetchSubjects(pageId);
    }

    public void initialisePresenter() {
        mPresenter = new SubjectsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void loadAdapter(SubjectsAdapter adapter) {
        recyclerViewSubjects.setVisibility(View.VISIBLE);
        recyclerViewSubjects.setAdapter(adapter);
        recyclerViewSubjects.setLayoutManager(new LinearLayoutManager(this));
        DividerItemDecoration itemDecor = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recyclerViewSubjects.addItemDecoration(itemDecor);
    }

    @Override
    public void goToTopicsActivity(int subjectId) {
        Intent intent = new Intent(this, TopicsActivity.class);
        intent.putExtra("subjectId", subjectId);
        startActivity(intent);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
