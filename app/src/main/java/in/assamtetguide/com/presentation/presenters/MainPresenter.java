package in.assamtetguide.com.presentation.presenters;

import in.assamtetguide.com.domain.model.MainData;

public interface MainPresenter {
    void getMainData();
    interface View {
        void loadData(MainData mainData);
    }
}
