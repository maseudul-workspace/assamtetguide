package in.assamtetguide.com.presentation.presenters;

import in.assamtetguide.com.presentation.ui.adapters.SubjectsAdapter;
import in.assamtetguide.com.presentation.ui.adapters.TopicsAdapter;

public interface TopicsPresenter {

    void fetchTopics(int pageNo, int subjectId, String type);

    interface View {
        void loadAdapter(TopicsAdapter topicsAdapter, int totalPage);
        void downloadFile(String fileName);
        void viewFile(String fileName);
        void hideProgressBar();
        void showPaginationProgressBar();
        void hidePaginationProgressBar();
    }

}
