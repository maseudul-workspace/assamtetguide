package in.assamtetguide.com.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.assamtetguide.com.domain.executor.Executor;
import in.assamtetguide.com.domain.executor.MainThread;
import in.assamtetguide.com.domain.interactors.FetchSubjectsInteractor;
import in.assamtetguide.com.domain.interactors.impl.FetchSubjectsInteractorImpl;
import in.assamtetguide.com.domain.model.Subjects;
import in.assamtetguide.com.presentation.presenters.SubjectsPresenter;
import in.assamtetguide.com.presentation.presenters.base.AbstractPresenter;
import in.assamtetguide.com.presentation.ui.adapters.SubjectsAdapter;
import in.assamtetguide.com.repository.AppRepositoryImpl;

public class SubjectsPresenterImpl extends AbstractPresenter implements SubjectsPresenter, FetchSubjectsInteractor.Callback, SubjectsAdapter.Callback {

    Context mContext;
    SubjectsPresenter.View mView;
    FetchSubjectsInteractorImpl interactor;
    SubjectsAdapter adapter;

    public SubjectsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchSubjects(int pageId) {
        interactor = new FetchSubjectsInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), pageId);
        interactor.execute();
    }

    @Override
    public void onFetchingSubjectsSuccess(Subjects[] subjects) {
        if (subjects.length == 0) {
            Toasty.warning(mContext, "No subjects found", Toast.LENGTH_LONG, true).show();
        } else {
            SubjectsAdapter adapter = new SubjectsAdapter(mContext, subjects, this::onViewClicked);
            mView.loadAdapter(adapter);
        }
        mView.hideProgressBar();
    }

    @Override
    public void onFetchingSubjectsFail(String errorMsg) {
        mView.hideProgressBar();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onViewClicked(int subjectId) {
        mView.goToTopicsActivity(subjectId);
    }
}
